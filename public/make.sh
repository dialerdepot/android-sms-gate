#!/bin/bash

set -e

export PATH=$PATH:$PWD/node_modules/.bin/

which minify || npm install minifier

which browserify || npm install browserify

[ ! -e node_modules ] && npm install

browserify index.js -o smsgate.js

minify jquery.viewbox.js
