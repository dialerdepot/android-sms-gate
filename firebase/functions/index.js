'use strict';

const functions = require('firebase-functions');
const admin = require('firebase-admin');
const firebase = admin.initializeApp();
const bucket = admin.storage().bucket();

const CUT_OFF_TIME = 6 * 30 * 24 * 60 * 60 * 1000; // 6 months in milliseconds.

exports.deleteOldUsers = functions.database.ref('users/{userId}').onCreate((snapshot, context) => {
  const ref = snapshot.ref.parent;
  const now = Date.now();
  const cutoff = now - CUT_OFF_TIME;
  const oldItemsQuery = ref.orderByChild('info/date').endAt(cutoff);
  return oldItemsQuery.once('value').then((snapshot) => {
    const updates = {};
    snapshot.forEach(child => { // create a map with all children that need to be removed
      console.log(child.key);
      bucket.deleteFiles({prefix: 'users/' + child.key});
      updates[child.key] = null;
    });
    return ref.update(updates); // execute all updates in one go and return the result to end the function
  });
});

exports.topics = functions.https.onRequest((req, res) => {
  res.set('Access-Control-Allow-Origin', '*');
  res.set('Access-Control-Allow-Methods', 'GET, PUT');
  res.set('Access-Control-Allow-Headers', 'Content-Type');

  if (req.method === 'OPTIONS') {
    return res.status(204).send('');
  }
    
  let message = req.body;

  if (!message) {
    return res.status(500).send("no message");
  }

  return admin.messaging().send(message).then((response) => {
    return res.status(200).send(response);
  })
  .catch((error) => {
    return res.status(500).send(error);
  });
});
