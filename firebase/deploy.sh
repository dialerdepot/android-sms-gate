#!/bin/bash

set -e

PATH=$PATH:$PWD/node_modules/.bin

which firebase || npm install firebase-tools

# firebase login

[ ! -e .firebaserc ] && firebase init

[ ! -e functions/node_modules ] && (cd functions && npm install)

firebase deploy
